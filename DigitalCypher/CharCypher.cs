﻿using System;
using System.Linq;
using System.Text;

namespace DigitalCypher
{
    public class CharCypher : Cypher<string, int[], int>
    {
        private const int AsciiIndex = 96;
        private readonly int[] _keyArray;

        public CharCypher(int key = 0) : base(key)
        {
            _keyArray = Key.ToString().Select(x => Convert.ToInt32(x.ToString())).ToArray();
        }

        public override int[] Encode(string input)
        {
            var source = Encoding.ASCII.GetBytes(input).Select(x => Convert.ToInt32(x) - AsciiIndex);

            return source.Select((x, index) => x + _keyArray[index % _keyArray.Length]).ToArray();
        }

        public override string Decode(int[] input)
        {
            var source = input.Select((x, index) => x - _keyArray[index % _keyArray.Length]);

            return new string(source.Select(x => Convert.ToChar(x + AsciiIndex)).ToArray());
        }
    }
}