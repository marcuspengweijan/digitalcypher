﻿using System;

namespace DigitalCypher
{
    public abstract class Cypher<TSource, TReturn, TKey> : ICypher<TSource, TReturn> where TSource : class where TReturn : class
    {
        protected readonly TKey Key;

        protected Cypher(TKey key) => Key = key;

        public virtual TSource Decode(TReturn input)
        {
            throw new NotImplementedException();
        }

        public virtual TReturn Encode(TSource input)
        {
            throw new NotImplementedException();
        }
    }
}