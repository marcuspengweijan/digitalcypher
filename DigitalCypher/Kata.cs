﻿using System;
using System.Linq;
using System.Text;

namespace DigitalCypher
{
    public class Kata
    {
        private const int AsciiIndex = 96;

        public static int[] Encode(string str, int n)
        {
            var key = GenerateKeyArray(n);
            var source = Encoding.ASCII.GetBytes(str).Select(x => Convert.ToInt32(x) - AsciiIndex);

            return source.Select((x, index) => x + key[index % key.Length]).ToArray();
        }

        public static string Decode(int[] input, int n)
        {
            var key = GenerateKeyArray(n);
            var source = input.Select((x, index) => x - key[index % key.Length]);

            return new string(source.Select(x => Convert.ToChar(x + AsciiIndex)).ToArray());
        }

        private static int[] GenerateKeyArray(int n)
        {
            return n.ToString().Select(x => Convert.ToInt32(x.ToString())).ToArray();
        }
    }
}
