﻿namespace DigitalCypher
{
    public interface ICypher<TSource, TReturn> where TSource : class where TReturn : class
    {
        TReturn Encode(TSource input);
        TSource Decode(TReturn input);
    }
}