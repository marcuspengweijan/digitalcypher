﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DigitalCypher.Tests
{
    [TestClass]
    public class KataTests
    {
        [DataTestMethod]
        [DataRow("scout", 1939, new[] { 20, 12, 18, 30, 21 }, DisplayName = "scout_1939")]
        [DataRow("masterpiece", 1939, new[] { 14, 10, 22, 29, 6, 27, 19, 18, 6, 12, 8 }, DisplayName = "masterpiece_1939")]
        [DataRow("abcde", 0, new[] { 1, 2, 3, 4, 5 }, DisplayName = "abcde_0")]
        [DataRow("aaa", 1, new[] { 2, 2, 2 }, DisplayName = "aaa_1")]
        [DataRow("abc", 12345, new[] { 2, 4, 6 }, DisplayName = "abc_12345")]
        public void EncodeTest(string str, int n, int[] expected)
        {
            CollectionAssert.AreEqual(expected, Kata.Encode(str, n));
        }

        [DataTestMethod]
        [DataRow(new[] { 20, 12, 18, 30, 21 }, 1939, "scout", DisplayName = "scout_1939")]
        [DataRow(new[] { 14, 10, 22, 29, 6, 27, 19, 18, 6, 12, 8 }, 1939, "masterpiece", DisplayName = "masterpiece_1939")]
        [DataRow(new[] { 1, 2, 3, 4, 5 }, 0, "abcde", DisplayName = "abcde_0")]
        [DataRow(new[] { 2, 2, 2 }, 1, "aaa", DisplayName = "aaa_1")]
        [DataRow(new[] { 2, 4, 6 }, 12345, "abc", DisplayName = "abc_12345")]
        public void DecodeTest(int[] input, int n, string expected)
        {
            Assert.AreEqual(expected, Kata.Decode(input, n));
        }
    }
}